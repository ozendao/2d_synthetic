mpiicc ExtrudeMeshRockIce.c -o ExtrudeMeshRockIce -lm
mkdir -p bin
elmerf90 -o bin/EnthalpySolver SRC/EnthalpySolver.f90
elmerf90 -o bin/HeatSolver SRC/HeatSolve.f90
elmerf90 -o bin/MassBalance SRC/TransientMassBalance_MaskRelief.F90 -fcheck=all
elmerf90 -o bin/Scalar_OUTPUT_Glacier SRC/Scalar_OUTPUT_Glacier_altMB.f90 -fcheck=all
elmerf90 -o bin/MassBalance_tr SRC/TransientMassBalance_MaskRelief_tr.F90 -fcheck=all
elmerf90 -o bin/NsVec SRC/IncompressibleNSVec.F90
elmerf90 -o bin/PercolSolver1D SRC/percol_1D_solver.f90
elmerf90 -o bin/IcyMaskSolver SRC/IcyMaskSolver.F90
elmerf90 -o bin/FreeSurface SRC/FreeSurfaceSolver.F90
elmerf90 -o bin/Compute2DNodalGradient SRC/Compute2DNodalGradient.F90

