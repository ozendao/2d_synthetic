import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
def SMB(simu_list,savename,savedir):
	matplotlib.rc('axes.formatter'  , limits=(-2,3))
	matplotlib.rc('lines'           , markersize=6.0)
	matplotlib.rc('scatter'         , marker="s")
	matplotlib.rc('xtick'           , labelsize=18)
	matplotlib.rc('ytick'           , labelsize=18)
	matplotlib.rc('axes'            , titlesize=20)
	matplotlib.rc('axes'            , labelsize=20)
	matplotlib.rc('legend'          , fontsize=16)
	matplotlib.rc('legend'          , title_fontsize=18)
	matplotlib.rc('legend'          , frameon=False)
	matplotlib.rc('text'            ,usetex=True)
	matplotlib.rc('figure'          ,titlesize=24)
	matplotlib.rc('savefig'         ,format='pdf')
	matplotlib.rc('savefig'         ,bbox='tight')
	data_m=[]
	labels_m=[]
	for key in simu_list:
		bflux_array=np.asarray([[float(k) for k in line.split()] for line in open("{1}_3d/bflux_{0}_{1}{2}_np1.dat".format('steady',key[0],key[1]),'r')])
		scout_array=np.asarray([[float(k) for k in line.split()] for line in open("{1}_3d/scout_{0}_{1}{2}.dat".format('steady',key[0],key[1]),'r')])
		data_m.append([scout_array,bflux_array])
		labels_m.append("{0}{1}".format(key[0],key[1]))

	ice_density=0.917
	fig3,axe3=plt.subplots(nrows=1,ncols=3,figsize=(24,12))
	fig3.suptitle(r'Initial stability')
	axe3[0].set_xlabel(r't(year)')
	axe3[1].set_xlabel(r't(year)')
	axe3[2].set_xlabel(r't(year)')
	axe3[0].set_ylabel(r'Mean height, V/A (m. eq.  water)')
	axe3[1].set_ylabel(r'Firn Front Position (m)')
	axe3[2].set_ylabel(r'Ice Front Position (m)')
	axe3[0].ticklabel_format(axis='x', style='sci')
	axe3[1].ticklabel_format(axis='x', style='sci')
	axe3[2].ticklabel_format(axis='x', style='sci')
	axe3[0].ticklabel_format(axis='y', style='sci')
	axe3[1].ticklabel_format(axis='y', style='sci')
	axe3[2].ticklabel_format(axis='y', style='sci')

	for tab,lab,key in zip (data_m,labels_m,simu_list):
		axe3[0].plot(tab[0][:,0],tab[0][:,1]*ice_density/tab[0][:,3],label=lab)
		axe3[1].plot(tab[0][:,0],tab[0][:,17],label=lab)
		axe3[2].plot(tab[0][:,0],tab[0][:,13],label=lab)


	axe3[0].legend(loc="lower right")
	axe3[1].legend(loc="lower right")
	axe3[2].legend(loc="lower right")
	fig3.savefig(savedir+'/IF'+savename+'.pdf')
	fig3.show()
