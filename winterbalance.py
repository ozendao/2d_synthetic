import numpy as np
import sys
import math


beginyear=1907
accubegin=0.25
accuend=0.33
icedensity=0.917
spy=int(sys.argv[4])
indbin=int(sys.argv[3])
scoutarray=np.asarray([[float(k) for k in line.split()] for line in open("{0}_3d/scout_secular_{0}{1}.dat".format(sys.argv[1],sys.argv[2]),'r')])
mean_list=[]
sys.stdout.write('winter accumulation in the bin centerd at '+str(scoutarray[0,indbin])+'\n') 
for year in range(2011,2023):
	indbegin=math.floor((year-accubegin-beginyear)*spy)
	indend=math.ceil((year+accuend-beginyear)*spy)
	wintersmb=0.0
	for i in range(indbegin,indend):
		wintersmb+=icedensity*scoutarray[i,indbin+1]/scoutarray[i,indbin+2]/spy
	mean_list.append(wintersmb)
	sys.stdout.write(str(year)+' '+str(wintersmb)+'\n')
sbm_mean=np.mean(np.asarray(mean_list))
sys.stdout.write('mean value '+str(sbm_mean)+'\n')
