import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#########################################
right=10.0
left=20.0
name1="r10m_trs_bedl_m4ep6"
name2="q3p1ref0304"
years=[1940,1955,1970,1985,2000,2007,2014,2021]
step_add=20
year_init=1907
spy=30
XMAX=500
result_file=name1+"_3d/outline_secular_"+name1+name2+".dat"
labels=["septembre "+str(ye) for ye in years] 
index=[math.floor((ye-year_init)*spy)+step_add for ye in years]
#print(index)
result=np.asarray([[ float(k) for k in line.split()] for line in open(result_file,'r')])
#########################################
matplotlib.rc('axes.formatter'  ,limits=(-2,3))
matplotlib.rc('lines'           ,markersize=6.0)
matplotlib.rc('scatter'         ,marker="s")
matplotlib.rc('xtick'           ,labelsize=18)
matplotlib.rc('ytick'           ,labelsize=18)
matplotlib.rc('axes'            ,titlesize=20)
matplotlib.rc('axes'            ,labelsize=20)
matplotlib.rc('legend'          ,fontsize=16)
matplotlib.rc('legend'          ,title_fontsize=18)
matplotlib.rc('legend'          ,frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)
matplotlib.rc('savefig'  ,format="pdf")
matplotlib.rc('savefig'  ,bbox="tight")
##########################################
figb,axeb=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figb.suptitle(r'Bed temperature')
axeb.set_ylabel(r'T (C)')
axeb.set_xlabel(r'Distance (m)')
axeb.set_xlim([0.0,XMAX])
#axeb.set_ylim([-2.0,0.5])
axeb.ticklabel_format(axis='x', style='sci')
axeb.ticklabel_format(axis='y', style='sci')
figb2,axeb2=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figb2.suptitle(r'Bed Slip Velocity')
axeb2.set_ylabel(r'$\Vert V \Vert$ (m/a)')
axeb2.set_xlabel(r'Distance (m)')
axeb2.set_xlim([0.0,XMAX])
#axeb2.set_ylim([-2.0,0.5])
axeb2.ticklabel_format(axis='x', style='sci')
axeb2.ticklabel_format(axis='y', style='sci')
figb3,axeb3=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figb3.suptitle(r'Bed Pressure')
axeb3.set_ylabel(r'P MPa')
axeb3.set_xlabel(r'Distance (m)')
axeb3.set_xlim([0.0,XMAX])
#axeb3.set_ylim([-2.0,0.5])
axeb3.ticklabel_format(axis='x', style='sci')
axeb3.ticklabel_format(axis='y', style='sci')
figs,axes=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figs.suptitle(r'Firn thickness')
axes.set_ylabel(r'thickness (m)')
axes.set_xlabel(r'Distance (m)')
axes.set_xlim([0.0,XMAX])
#axes.set_ylim([0.0,30])
axes.ticklabel_format(axis='x', style='sci')
axes.ticklabel_format(axis='y', style='sci')
###########################################
for key,ind in zip(labels,index):
	mask_bl=(result[:,0]==ind)*(result[:,2]==2)*(result[:,5]==left) 
	mask_br=(result[:,0]==ind)*(result[:,2]==2)*(result[:,5]==right)
	mask_sl=(result[:,0]==ind)*(result[:,2]==3)*(result[:,5]==left) 
	mask_sr=(result[:,0]==ind)*(result[:,2]==3)*(result[:,5]==right)
	axeb.plot(result[mask_bl,4],0.5*(result[mask_bl,8]+result[mask_br,8]),label=key)
	axeb2.plot(result[mask_bl,4],0.5*((result[mask_bl,9]**2+result[mask_bl,11]**2)**0.5+(result[mask_br,9]**2+result[mask_br,11]**2)**0.5),label=key)
	axeb3.plot(result[mask_bl,4],0.5*(result[mask_bl,12]+result[mask_br,12]),label=key)
	axes.plot(result[mask_sl,4],0.5*(result[mask_sl,13]+result[mask_sr,13]),label=key)
figb.show()
figb2.show()
figb3.show()
figs.show()
figb.legend()
figb2.legend()
figb3.legend()
figs.legend()
figb.savefig("BT_"+name1+name2+".pdf")
figb2.savefig("BV_"+name1+name2+".pdf")
figb3.savefig("BP_"+name1+name2+".pdf")
figs.savefig("Firn_"+name1+name2+".pdf")
#######################################
input()	
