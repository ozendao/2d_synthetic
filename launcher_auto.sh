dir_glacier=$6
dir_dat=$dir_glacier/dat
dir_out=$dir_glacier/out
dir_set=$dir_glacier/setsh
mkdir -p $dir_glacier
mkdir -p $dir_dat
mkdir -p $dir_out
mkdir -p $dir_set
echo "data_names $dir_dat/$1.dat $dir_out/mesh_$1$2 $dir_out/$1$2_3d" > $dir_out/$1$2
XMAX=$(tail -n 1 $dir_dat/$1.dat | cut -d " " -f1)
ZMIN=$(tail -n 1 $dir_dat/$1.dat | cut -d " " -f2)
ZMAX=$(sed 1q $dir_dat/$1.dat | cut -d " " -f2)
STEP=$(grep "el_size" $dir_glacier/$2 | cut -d " " -f2)
XLEN=$(expr $XMAX-$STEP|bc)
YWIDTH=$(expr 3*$STEP|bc)
YSCOUT=$(expr 1.5*$STEP|bc)
SCB=$(expr $XMAX/5|bc)
echo "XMAX=$XMAX ZMAX=$ZMAX ZMIN=$ZMIN"
echo "mesh_limits 0.0 $XLEN 0.0 $YWIDTH" >> $dir_out/$1$2
cat $dir_glacier/$2 >> $dir_out/$1$2
python3 profil2mesh_auto.py $dir_out $1$2
echo "DIR=\"$1$2_3d\"" > $dir_set/stesec$1$2$3$4.sh
cat $dir_set/modsete_$3.sh   >> $dir_set/stesec$1$2$3$4.sh
cat $dir_set/keepsete_$4.sh    >> $dir_set/stesec$1$2$3$4.sh
cat $dir_out/mesh_$1$2/maskindex >> $dir_set/stesec$1$2$3$4.sh
echo "YSC=$YSCOUT" >> $dir_set/stesec$1$2$3$4.sh
echo "XSC=$XMAX" >> $dir_set/stesec$1$2$3$4.sh
echo "SCBZ=$SCB" >> $dir_set/stesec$1$2$3$4.sh
echo "scoutstrz=\"SMB altitude Range (2) = real $ZMIN $ZMAX\"" >> $dir_set/stesec$1$2$3$4.sh
bash sifbuilder_auto.sh $dir_set $dir_out $1$2$3$4 $5
#STSTE=$(grep "SPYSTE=" $dir_set/keepsete_$4.sh | cut -d "=" -f2)
#STSEC=$(grep "SPYSEC=" $dir_set/keepsete_$4.sh | cut -d "=" -f2)
#echo "python3 autoplot.py $1$2 $3$4$5 $STSEC $STSTE">> exec$1$2$3$4$5.sh
