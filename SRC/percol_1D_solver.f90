SUBROUTINE percol_1D_solver(Model, Solver, dt, Transient)
Use DefUtils
IMPLICIT NONE
TYPE(Solver_t) :: Solver
TYPE(Model_t) :: Model
TYPE(ValueList_t), POINTER :: BC,Material,SolverParams
TYPE(Element_t), POINTER :: Element 
INTEGER, POINTER :: NodeIndexes(:)
TYPE(variable_t), POINTER :: Enth,Hs,Depth,SE,Damage,Qlat,ThickTemp,Densrel
REAL(KIND=dp) :: dt,dz,Sr,rho_w,rho_ice,L_heat,wres,H_lim,cont,NewEnth,PercolCrevDamage,SurfEnthBoundaryDepth,yearinsec,Dlimit
LOGICAL :: Transient,GotIt,Found,PercolBottom
INTEGER :: N,i,j,nb,k,Nsurf
real,dimension(:),allocatable ::fonte
REAL(KIND=dp), ALLOCATABLE ::fonte_loc(:)
logical :: first_time=.true.,PercolCrev=.false.
SAVE first_time,Sr,rho_w,rho_ice,L_heat,N,Nsurf,PercolCrev,PercolCrevDamage,SurfEnthBoundaryDepth,Dlimit

yearinsec=3600*24*365.25

if (first_time) then
  first_time=.false.

N = Model % NumberOfNodes

!========Get number of surface node===================================================

Depth => VariableGet( Model % Variables, 'Depth')
Nsurf=0
DO i=1,N
IF (Depth % values (Depth % perm (i))==0.0) THEN
Nsurf=Nsurf+1
ENDIF
ENDDO


rho_w=GetConstReal(Model % Constants, "rho_water")/(1.0e6*yearinsec**2)
rho_ice=GetConstReal(Model % Constants, "rho_ice")/(1.0e6*yearinsec**2)
Sr=GetConstReal(Model % Constants, "Residual Saturation")
L_heat=GetConstReal(Model % Constants, "L_heat")
PercolCrev=GetLogical(Model % Constants, "PercolCrev")
if (Percolcrev) then
PercolCrevDamage=GetConstReal(Model % Constants, "PercolCrevDamage")
endif
SurfEnthBoundaryDepth=GetConstReal(Model % Constants, "SurfEnthBoundaryDepth")
Dlimit=GetConstReal(Model % Constants, "Percolation limit",GotIt)
if(.NOT. GotIT) then
Dlimit=0.9
endif
endif

!==========Density and capacity from materials=======================================



do i=1,Solver % NumberOfActiveElements

   Element => GetActiveElement(i)
   nb = GetElementNOFNodes(Element)
   NodeIndexes => Element % NodeIndexes
   Material => GetMaterial(Element)
   
enddo


!======Get enthalpy and depth================================================

Hs => VariableGet( Model % Variables, 'Phase Change Enthalpy' )
Enth => VariableGet( Model % Variables, 'enthalpy_h')
Qlat => VariableGet( Model % Variables, 'Qlat')
Depth => VariableGet( Model % Variables, 'Depth')
Damage => VariableGet( Model % Variables, 'Damage')
Densrel => VariableGet(Model % Variables, 'Densrel')
!===================================================================================
!========Get surface melting=========================================================
ALLOCATE(fonte_loc(Model % MaxElementNodes))
ALLOCATE(fonte(N)) 

fonte=0.0
fonte_loc=0.0

DO i=1,Model % NumberOfBoundaryElements

   Element => GetBoundaryElement(i)
   nb = GetElementNOFNodes(Element)
   NodeIndexes => Element % NodeIndexes

   BC => GetBC(Element)

   fonte_loc(1:nb) = ListGetReal( BC,'Surf_melt', nb, NodeIndexes,GotIt)
   IF (.NOT.GotIt) THEN
	!fonte(NodeIndexes)=0.0
   ELSE
        fonte(NodeIndexes)=fonte_loc(1:nb)
   END IF

ENDDO

!Initialize Qlat===========================================================================

do i=1,N
Qlat % values (Qlat % perm (i))=0.0
enddo

!===========================================================================================
!=========Loop over surface nodes=========================================================
DO i=1,Nsurf
!Surface melting en kg m-2:
cont=fonte(N+1-i)*rho_w
j=0
PercolBottom=.false.

!=========Loop over verticaly aligned nodes ===============================================
DO WHILE (cont>0.0)


!Curent node number:
	k = N+1-i-j*Nsurf

	IF (SurfEnthBoundaryDepth>Depth % values (Depth % perm (k)))  THEN
		j=j+1
		CYCLE
	ENDIF
	if (PercolCrev) then 
	IF (Damage % values (Damage % perm (k))>PercolCrevDamage) THEN
		PercolBottom=.true.
	ENDIF
    end if
!layer thickness:
	dz = Depth % values (Depth % perm (k-Nsurf))-Depth % values (Depth % perm (k))
	
	IF (((PercolBottom).and.(PercolCrev)).or.(Densrel % values (Densrel % perm (k))<Dlimit)) THEN

!===========Residual water content========================================================

		wres=Sr*rho_w*(1.0-Densrel % values (Densrel % perm (k)))
		H_lim = Hs % values (Hs % perm (k)) + wres/(Densrel % values (Densrel % perm (k))*rho_ice)*L_heat

!================percolation ============================================================

		NewEnth = Enth % values (Enth % perm (k)) + L_heat*cont/dz/(Densrel % values (Densrel % perm (k))*rho_ice)

		if (NewEnth > H_lim) then
			cont=cont-(H_lim-Enth % values (Enth % perm (k)))*dz*(Densrel % values (Densrel % perm (k))*rho_ice)/L_heat
			Qlat % values (Qlat % perm (k))= (H_lim-Enth % values (Enth % perm (k)))/dt
		else
			Qlat % values (Qlat % perm (k))=  L_heat*cont/dz/(Densrel % values (Densrel % perm (k))*rho_ice)/dt
            cont=0.0
		endif

	ELSE

	cont=0.0

	ENDIF

	j=j+1

	if (j==Model % NumberOfNodes/Nsurf-1) then
		cont=0.0
	endif
	if (Qlat % values (Qlat % perm (k))<0.0) then
		Qlat % values (Qlat % perm (k))=0.0
	endif

ENDDO 
ENDDO !surface node


END SUBROUTINE percol_1D_solver
