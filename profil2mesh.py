import numpy as np
import os,sys,math
#####################################################
############ Reading the input file #################
#####################################################
inputfilename=sys.argv[1]##### a name without extension please
maskaccudimfile="mesh_"+inputfilename+"/maskindex"
for line in open(inputfilename,'r'):
	wlist=line.split()
	print(wlist)
	if wlist[0]=="data_names":
		profilname=wlist[1]
		meshdir=wlist[2]
		mesh_extruded=wlist[3]
	elif wlist[0]=="thickness":
		rock_thick=float(wlist[1])
		ice_thick=float(wlist[2])
	elif wlist[0]=="el_size":
		el_size=float(wlist[1])
	elif wlist[0]=="mesh_extruder":
		mesh_extruder=wlist[1]
	elif wlist[0]=="nb_levels":
		nb_levels_rock=int(wlist[1])
		nb_levels_ice=int(wlist[2])
	elif wlist[0]=="GL":
		GL_b_rock=int(wlist[1])
		GL_s_rock=int(wlist[2])
		GL_b_ice=int(wlist[3])
		GL_s_ice=int(wlist[4])
	elif wlist[0]=="ratio":
		ratio_b_rock=float(wlist[1])
		ratio_s_rock=float(wlist[2])
		ratio_b_ice=float(wlist[3])
		ratio_s_ice=float(wlist[4])
	elif wlist[0]=="mesh_limits":
		mesh_xmin=float(wlist[1])
		mesh_xmax=float(wlist[2])
		mesh_ymin=float(wlist[3])
		mesh_ymax=float(wlist[4])
	elif wlist[0]=="steps":
		mesh_xstep=float(wlist[1])
		mesh_ystep=float(wlist[2])
	else:
		print("######## bad keyword ########\n######## Stopping    ########\n")
		raise("Incorrect input file")
##### entries with useless modification ######
seqindex=[1,2,3]
cutoff=1.2*el_size
wexp=1.5
noval=-9999
nb_proc=1
min_depth_rock=rock_thick/5
min_depth_ice=ice_thick/5
extrude_config=meshdir+"/extrude_config"
mesh2D=meshdir+"/"+"footprint"
#########################################################
########## Making interface rock/ice from profil
########################################################
contour_list=[]
contour_list.append([mesh_xmin+el_size,mesh_ymin+el_size])
contour_list.append([mesh_xmin+el_size,mesh_ymax-el_size])
contour_list.append([mesh_xmax-el_size,mesh_ymax-el_size])
contour_list.append([mesh_xmax-el_size,mesh_ymin+el_size])
Contour=np.asarray(contour_list)
###################
profil_list=[[float(k) for k in line.split()] for line in open(profilname,'r')]
profil_list.reverse()
xlist=[mesh_xmin+k*mesh_xstep for k in range(math.floor((mesh_xmax-mesh_xmin)/mesh_xstep)+1)]
###################
x=mesh_xmin
z=profil_list[-1][1]
pc=profil_list[-1][2]
alpha=0.0
########
x_variation=[]
for xk in xlist:
	if (profil_list==[]):
		alpha=0.0
	elif (xk>=profil_list[-1][0]):
		xzlist=profil_list.pop()
		x=xzlist[0]
		z=xzlist[1]
		pc=xzlist[2]
		if (profil_list==[]):
			alpha=0.0
		else:
			alpha=(z-profil_list[-1][1])/(x-profil_list[-1][0])
			beta=(pc-profil_list[-1][2])/(x-profil_list[-1][0])
	x_variation.append([xk,mesh_ymin,z+alpha*(xk-x),pc+beta*(xk-x)])
#########
xlen=len(x_variation)
copy_array=np.asarray(x_variation)
interface=np.copy(copy_array[:,0:3])
maskaccu=np.copy(copy_array[:,(0,1,3)])
for k in range(math.floor((mesh_ymax-mesh_ymin)/mesh_ystep)):
	copy_array[:,1]+=mesh_ystep
	interface=np.concatenate((interface,np.copy(copy_array[:,0:3])))
	maskaccu=np.concatenate((maskaccu,np.copy(copy_array[:,(0,1,3)])))
ylen=math.floor(len(interface)/xlen)
#####################################
###### creating input files for extrude mesh
####################################
################ interface bed and surf
os.system("mkdir -p "+meshdir)
os.system("mkdir -p "+mesh_extruded)
madim=open(maskaccudimfile,'w')
madim.write("MACX={0:d}\nMACY={1:d}\n".format(xlen,ylen))
madim.close()
np.savetxt(meshdir+"/contour.xy",Contour)
np.savetxt(meshdir+"/interface.xyz",interface)
np.savetxt(mesh_extruded+"/maskaccu.dat",maskaccu)
bed=np.copy(interface)
surf=np.copy(interface)
for i in range(len(interface)):
	if bed[i,2]>noval:
		bed[i,2]-=rock_thick
	if surf[i,2]>noval:
		surf[i,2]+=ice_thick
np.savetxt(meshdir+"/bed.xyz",bed)
np.savetxt(meshdir+"/surf.xyz",surf)
################# 2D mesh  for gmsh
x = Contour[:,0]
y = Contour[:,1]
Npt = len(x)    
grd = open(mesh2D+".grd", 'w')
grd.write("Version = 210903\nCoordinate System = Cartesian 2D\nSubcell Divisions in 2D = 1 1\n")  
grd.write("Subcell Limits 1 = {0:f} {1:f}\nSubcell Limits 2 = {2:f}  {3:f}\n".format(Contour[0,0],Contour[2,0],Contour[0,1],Contour[1,1]))
grd.write("Material Structure in 2D\n  1\nEnd\n")
grd.write("Boundary Definitions\n  1 -4 1 1\n  2 -3 1 1\n  3 -2 1 1\n  4 -1 1 1\nEnd\n")
grd.write("Numbering = Horizontal\nCoordinate Ratios = 1\nElement Degree = 1\nTriangles = True\n")
grd.write("Element Divisions 1 = {0:d}\nElement Divisions 2 = {1:d}\n".format(math.ceil((Contour[2,0]-Contour[0,0])/el_size),math.ceil((Contour[1,1]-Contour[0,1])/el_size)))
grd.close()
################## config file
EM_config=open(extrude_config,'w')
EM_config.write("{0:d} {1:d} {2:f} {3:f} 1\n".format(nb_levels_rock,nb_levels_ice,min_depth_rock,min_depth_ice))
EM_config.write("0\n")
EM_config.write("{0:d} {1:d} {2:d} {3:d} {4:f} {5:f} {6:f} {7:f} 0 0 0 0\n".format(GL_b_rock,GL_s_rock,GL_b_ice,GL_s_ice,ratio_b_rock,ratio_s_rock,ratio_b_ice,ratio_s_ice))
EM_config.write("{0:f} {1:f} {2:f}\n".format(cutoff,wexp,noval))
EM_config.write("0\n")
EM_config.close()
######### Creating the 2D footprint
os.system("ElmerGrid 1 2 {0} -autoclean".format(grd.name))
os.system("ElmerGrid 1 5 {0} -autoclean".format(grd.name))
######### Extrusion
os.system(mesh_extruder+" "+mesh2D+" "+mesh_extruded+" "+extrude_config+" "+meshdir)
os.system("ElmerGrid 2 5 "+mesh_extruded+" -autoclean")	
######### Visualisation sif file
lkey=["upper side","left bank","outlet","right bank"]
sif_visu=open("visu_{0}.sif".format(inputfilename),'w')
sif_visu.write("check keywords warn\necho on\n")
sif_visu.write("$Step = \"{0}_visu\"\n".format(inputfilename))
sif_visu.write("Header\n   Mesh DB \".\" \"{0}\"\nEnd\n".format(mesh_extruded))
sif_visu.write("Simulation\n   Coordinate System  =  Cartesian 3D\n   Simulation Type = Steady\n")
sif_visu.write("  Timestep Intervals = 1\n  Timestep Sizes = 1\n  Output Intervals = 1\n")
sif_visu.write("  Steady State Min Iterations = 1\n  Steady State Max Iterations = 1\n  Output File = \"$Step\".result\"\n")
sif_visu.write("  Post File = \"$Step\".vtu\"\nEnd\n")
sif_visu.write("Body 1\n  Equation = 1\n  Body Force = 1\n  Material = 1\n  Initial Condition = 1\nEnd\n")
sif_visu.write("Body 2\n  Equation = 1\n  Body Force = 1\n  Material = 1\n  Initial Condition = 1\nEnd\n")
sif_visu.write("Body 3\n  Equation = 2\n  Body Force = 1\n  Material = 1\n  Initial Condition = 1\nEnd\n")
sif_visu.write("Initial Condition 1\nEnd\n")
sif_visu.write("Body Force 1\nEnd\n")
sif_visu.write("Material 1\nEnd\n")
sif_visu.write("Solver 1\n  Equation = \"Flowdepth\"\n  Procedure = File \"ElmerIceSolvers\" \"FlowDepthSolver\"\n")
sif_visu.write("  Variable = String \"Thick\"\n  Variable DOFs = 1\n  Linear System Solver = \"Direct\"\n")
sif_visu.write("  Gradient = Real 1.0\nEnd\n")
sif_visu.write("Solver 2\n  Equation = \"export Zbed\"\n  Procedure = File \"ElmerIceSolvers\" \"ExportVertically\"\n")
sif_visu.write("  Variable = String \"Zbed\"\nVariable DOFs = 1\n  Linear System Solver = \"Direct\"\nEnd\n")
sif_visu.write("Solver 3\n  Equation = \"bed gradient\"\n  Variable = -dofs 2 SurfGrad\n")
sif_visu.write("  Procedure = \"bin/Compute2DNodalGradient\" \"Compute2DNodalGradient\"\n")
sif_visu.write("  Optimize Bandwidth = False\n  Variable Name = string \"Zbed\"\n")
sif_visu.write("  FE consistent average = Logical True\n  Linear System Solver = \"Direct\"\nEnd\n")
sif_visu.write("Equation 1\n  Active Solvers(2) = 1 2\nEnd\n")
sif_visu.write("Equation 2\n  Active Solvers(1) = 3\nEnd\n")
sif_visu.write("! ground\nBoundary Condition 1\n  Target Boundaries = 1\nEnd\n")
sif_visu.write("! interface=bedrock\nBoundary Condition 2\n  Target Boundaries = 2\n")
sif_visu.write("  thick=Real 0.0\n  Zbed=Equals coordinate 3\nEnd\n")
sif_visu.write("!ice free surface\n  Boundary Condition 3\n  Target Boundaries = 3\n  Body Id = 3\nEnd\n")
for i,k in zip(range(len(lkey)),lkey):
	sif_visu.write("! lateral rock border {0}\nBoundary Condition {1:0d}\n  Target Boundaries ={1:0d}\nEnd\n".format(k,4+2*i))
	sif_visu.write("! lateral ice border {0}\nBoundary Condition {1:0d}\n  Target Boundaries = {1:0d}\nEnd\n".format(k,4+2*i+1))
sif_visu.close()
os.system("ElmerSolver "+sif_visu.name) 
