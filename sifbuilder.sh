source stesec$1.sh

SUFFIX=$1$2
strste=(\
"#name=\"steady_${SUFFIX}\"" \
"#dir=\"${DIR}\"" \
"#Radf=${RF}" \
"#Albi=${AI}" \
"#Albs=${AS}" \
"#ks=${KS}" \
"#ki=${KI}" \
"#k0=${K0}" \
"#fp=${FP}" \
"#Asp=${ASP}" \
"#GT=${GT}" \
"#GP=${GP}" \
"#TC=${TC}" \
"#PC=${PC}" \
"#WFC=${WFC}" \
"#WFT=${WFT}" \
"#WE=${WE}" \
"#OInt=${OISTE}" \
"#FG=${FG}" \
"#stokes=${STO}" \
"#xref_scout=${XSC}" \
"#yref_scout=${YSC}" \
"#nyear=${NY}" \
"#maxit=${MISTE}" \
"#steps_per_year=${SPYSTE}" \
"#delta_temp=${DET}" \
"#minh=${MHSTE}" \
"#scoutbnz=${SCBZ}" \
"#scalmt=${SCM}" \
"#macx=${MACX}" \
"#macy=${MACY}" \
"#scoutint=${SCOT}"\
)
strsec=(\
"#name=\"secular_${SUFFIX}\"" \
"#dir=\"${DIR}\"" \
"#Radf=${RF}" \
"#Albi=${AI}" \
"#Albs=${AS}" \
"#ks=${KS}" \
"#ki=${KI}" \
"#k0=${K0}" \
"#fp=${FP}" \
"#Asp=${ASP}" \
"#GT=${GT}" \
"#GP=${GP}" \
"#TC=${TC}" \
"#PC=${PC}" \
"#WFC=${WFC}" \
"#WFT=${WFT}" \
"#WE=${WE}" \
"#OInt=${OISEC}" \
"#FG=${FG}" \
"#stokes=${STO}" \
"#resinit=\"steady_${SUFFIX}.result\"" \
"#posinit=${PI}" \
"#timeinit=${TI}" \
"#xref_scout=${XSC}" \
"#yref_scout=${YSC}" \
"#maxit=${MISEC}" \
"#steps_per_year=${SPYSEC}" \
"#delta_temp=${DET}" \
"#minh=${MHSEC}" \
"#scoutbnz=${SCBZ}" \
"#scalmt=${SCM}" \
"#macx=${MACX}" \
"#macy=${MACY}" \
"#scoutint=${SCOC}"\
)
keyste=(\
"#name=" \
"#dir=" \
"#Radf=" \
"#Albi=" \
"#Albs=" \
"#ks=" \
"#ki=" \
"#k0=" \
"#fp=" \
"#Asp=" \
"#GT=" \
"#GP=" \
"#TC=" \
"#PC=" \
"#WFC=" \
"#WFT=" \
"#WE=" \
"#OInt=" \
"#FG=" \
"#stokes=" \
"#xref_scout=" \
"#yref_scout=" \
"#nyear=" \
"#maxit=" \
"#steps_per_year=" \
"#delta_temp=" \
"#minh=" \
"#scoutbnz=" \
"#scalmt=" \
"#macx=" \
"#macy=" \
"#scoutint="\
)
keysec=(\
"#name=" \
"#dir=" \
"#Radf=" \
"#Albi=" \
"#Albs=" \
"#ks=" \
"#ki=" \
"#k0=" \
"#fp=" \
"#Asp=" \
"#GT=" \
"#GP=" \
"#TC=" \
"#PC=" \
"#WFC=" \
"#WFT=" \
"#WE=" \
"#OInt=" \
"#FG=" \
"#stokes=" \
"#resinit=" \
"#posinit=" \
"#timeinit=" \
"#xref_scout=" \
"#yref_scout=" \
"#maxit=" \
"#steps_per_year=" \
"#delta_temp=" \
"#minh=" \
"#scoutbnz=" \
"#scalmt=" \
"#macx=" \
"#macy=" \
"#scoutint="\
)
LEN=$(echo ${#keyste[@]})
LENS=$(echo ${#keysec[@]})
scoutkeyz=$(grep "SMB altitude Range" steady_$2.sif)
cat steady_$2.sif | sed -e "s/${scoutkeyz}/${scoutstrz}/" > steady_$1$2.sif
scoutkeyz=$(grep "SMB altitude Range" secular_$2.sif)
cat secular_$2.sif | sed -e "s/${scoutkeyz}/${scoutstrz}/" > secular_$1$2.sif

for (( i=0; i<LEN; i++ ))
do
	tmpkey=$(grep ${keyste[$i]} steady_$2.sif)
	sed -e "s/${tmpkey}/${strste[$i]}/" -i steady_$1$2.sif 
done
for (( i=0; i<LENS; i++ ))
do
	tmpkey=$(grep ${keysec[$i]} secular_$2.sif)
	sed -e "s/${tmpkey}/${strsec[$i]}/" -i secular_$1$2.sif 
done

echo "ElmerSolver steady_$1$2.sif" > exec$1$2.sh 
echo "ElmerSolver secular_$1$2.sif" >> exec$1$2.sh 
