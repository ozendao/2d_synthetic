echo "data_names $1.dat mesh_$1$2 $1$2_3d" > $1$2
XMAX=$(tail -n 1 $1.dat | cut -d " " -f1)
ZMIN=$(tail -n 1 $1.dat | cut -d " " -f2)
ZMAX=$(sed 1q $1.dat | cut -d " " -f2)
STEP=$(grep "el_size" $2 | cut -d " " -f2)
XLEN=$(expr $XMAX-$STEP|bc)
YWIDTH=$(expr 3*$STEP|bc)
YSCOUT=$(expr 1.5*$STEP|bc)
SCB=$(expr $XMAX/5|bc)
echo "XMAX=$XMAX ZMAX=$ZMAX ZMIN=$ZMIN"
echo "mesh_limits 0.0 $XLEN 0.0 $YWIDTH" >> $1$2
cat $2 >> $1$2
python3 profil2mesh.py $1$2
echo "DIR=\"$1$2_3d\"" > stesec$1$2$3$4.sh
cat modsete_$3.sh   >> stesec$1$2$3$4.sh
cat keepsete_$4.sh    >> stesec$1$2$3$4.sh
cat mesh_$1$2/maskindex >> stesec$1$2$3$4.sh
echo "YSC=$YSCOUT" >> stesec$1$2$3$4.sh
echo "XSC=$XMAX" >> stesec$1$2$3$4.sh
echo "SCBZ=$SCB" >> stesec$1$2$3$4.sh
echo "scoutstrz=\"SMB altitude Range (2) = real $ZMIN $ZMAX\"" >> stesec$1$2$3$4.sh
bash sifbuilder.sh $1$2$3$4 $5
STSTE=$(grep "SPYSTE=" keepsete_$4.sh | cut -d "=" -f2)
STSEC=$(grep "SPYSEC=" keepsete_$4.sh | cut -d "=" -f2)
echo "python3 autoplot.py $1$2 $3$4$5 $STSEC $STSTE">> exec$1$2$3$4$5.sh
