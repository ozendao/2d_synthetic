import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
def SMB(simu_list,savename,savedir):
	matplotlib.rc('axes.formatter'  , limits=(-2,3))
	matplotlib.rc('lines'           , markersize=6.0)
	matplotlib.rc('scatter'         , marker="s")
	matplotlib.rc('xtick'           , labelsize=18)
	matplotlib.rc('ytick'           , labelsize=18)
	matplotlib.rc('axes'            , titlesize=20)
	matplotlib.rc('axes'            , labelsize=20)
	matplotlib.rc('legend'          , fontsize=16)
	matplotlib.rc('legend'          , title_fontsize=18)
	matplotlib.rc('legend'          , frameon=False)
	matplotlib.rc('text'            ,usetex=True)
	matplotlib.rc('figure'          ,titlesize=24)
	matplotlib.rc('savefig'         ,format='pdf')
	matplotlib.rc('savefig'         ,bbox='tight')
	data_m=[]
	labels_m=[]
	for key in simu_list:
		bflux_array=np.asarray([[float(k) for k in line.split()] for line in open("{1}_3d/bflux_{0}_{1}{2}_np1.dat".format('secular',key[0],key[1]),'r')])
		scout_array=np.asarray([[float(k) for k in line.split()] for line in open("{1}_3d/scout_{0}_{1}{2}_total.dat".format('secular',key[0],key[1]),'r')])
		data_m.append([scout_array,bflux_array])
		labels_m.append("{0}{1}".format(key[0],key[1]))

	ice_density=0.917
	fig1,axe1=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
	fig1.suptitle(r'Mass Evolution')
	axe1.set_xlabel(r't(year)') 
	axe1.set_ylabel(r'Mean height (m. eq.  water)')
	axe1.ticklabel_format(axis='x', style='sci')
	axe1.ticklabel_format(axis='y', style='sci')
	fig2,axe2=plt.subplots(nrows=1,ncols=2,figsize=(24,12))
	fig2.suptitle(r'Relative Area')
	axe2[0].set_xlabel(r't(year)')
	axe2[1].set_xlabel(r't(year)')
	axe2[0].set_ylabel(r'(\%)')
	axe2[1].set_ylabel(r'(\%)')
	axe2[0].ticklabel_format(axis='x', style='sci')
	axe2[1].ticklabel_format(axis='x', style='sci')
	axe2[0].ticklabel_format(axis='y', style='sci')
	axe2[1].ticklabel_format(axis='y', style='sci')
	fig3,axe3=plt.subplots(nrows=2,ncols=2,figsize=(24,24))
	fig3.suptitle(r'Front evolution')
	axe3[0,0].set_xlabel(r't(year)')
	axe3[1,0].set_xlabel(r't(year)')
	axe3[0,1].set_xlabel(r't(year)')
	axe3[1,1].set_xlabel(r't(year)')
	axe3[0,0].set_ylabel(r'Front Altutide (m)')
	axe3[1,0].set_ylabel(r'Front Position (m)')
	axe3[0,1].set_ylabel(r'Front Altutide (m)')
	axe3[1,1].set_ylabel(r'Front Position (m)')
	axe3[0,0].ticklabel_format(axis='x', style='sci')
	axe3[1,0].ticklabel_format(axis='x', style='sci')
	axe3[0,1].ticklabel_format(axis='x', style='sci')
	axe3[1,1].ticklabel_format(axis='x', style='sci')
	axe3[0,0].ticklabel_format(axis='y', style='sci')
	axe3[1,0].ticklabel_format(axis='y', style='sci')
	axe3[0,1].ticklabel_format(axis='y', style='sci')
	axe3[1,1].ticklabel_format(axis='y', style='sci')


	for tab,lab,key in zip (data_m,labels_m,simu_list):
		stepsperyear=key[2]
		axe1.plot(tab[0][:,0],tab[0][:,1]*ice_density/tab[0][:,3],label="Volume "+lab)
		axe2[0].plot(tab[0][:,0],100*tab[0][:,6]/tab[0][:,3],label="Firn "+lab)
		axe2[1].plot(tab[0][:,0],100*tab[0][:,7]/tab[0][:,3],label="Bottom temperate "+lab)
		axe3[0,0].plot(tab[0][:,0],tab[0][:,12],label="Ice Front "+lab)
		axe3[0,1].plot(tab[0][:,0],tab[0][:,16],label="Firn Front "+lab)
		axe3[1,0].plot(tab[0][:,0],tab[0][:,13],label="Ice Front "+lab)
		axe3[1,1].plot(tab[0][:,0],tab[0][:,17],label="Firn Front "+lab)
		cumul_dzdt=np.zeros(tab[0].shape[0])
		cumul_MB=np.zeros(tab[0].shape[0])
		cumul_flow=np.zeros(tab[1].shape[0])
		cumul_load=np.zeros(tab[0].shape[0])
		prev_m=0
		prev_f=0
		prev_l=0
		prev_dzdt=0
		for i in range(tab[0].shape[0]):
			cumul_dzdt[i]=prev_dzdt+((tab[0][i,2]*ice_density)/tab[0][i,3])/stepsperyear
			prev_dzdt+=((tab[0][i,2]*ice_density)/tab[0][i,3])/stepsperyear			
			cumul_MB[i]=prev_m+((tab[0][i,8]*ice_density)/tab[0][i,3])/stepsperyear
			prev_m+=((tab[0][i,8]*ice_density)/tab[0][i,3])/stepsperyear
			cumul_flow[i]=prev_f+((tab[1][i,3]*ice_density)/tab[0][i,3])/stepsperyear
			prev_f+=((tab[1][i,3]*ice_density)/tab[0][i,3])/stepsperyear
			cumul_load[i]=prev_l+(tab[0][i,11]*ice_density/tab[0][i,3])/stepsperyear
			prev_l+=(tab[0][i,11]*ice_density/tab[0][i,3])/stepsperyear

		axe1.plot(tab[0][:,0],cumul_dzdt+tab[0][0,1]/tab[0][0,3],label="cumul dzdt "+lab)
		axe1.plot(tab[0][:,0],cumul_MB+tab[0][0,1]/tab[0][0,3],label="cumul MB "+lab)
		axe1.plot(tab[0][:,0],cumul_MB-cumul_load-cumul_flow+tab[0][0,1]/tab[0][0,3],label="cumul MB-loads-flow "+lab)

	axe1.legend(loc="lower left")
	axe2[0].legend(loc="upper right")
	axe2[1].legend(loc="upper right")
	axe3[0,0].legend(loc="lower left")
	axe3[1,0].legend(loc="lower left")
	axe3[0,1].legend(loc="lower left")
	axe3[1,1].legend(loc="lower left")
	fig1.savefig(savedir+'/MB'+savename+'.pdf')
	fig2.savefig(savedir+'/RA'+savename+'.pdf')
	fig3.savefig(savedir+'/FE'+savename+'.pdf')
	fig1.show()
	fig2.show()
	fig3.show()

