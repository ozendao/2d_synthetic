set title 'basal temperature r10m\_trs\_bedl\_m4ep4w5v3u6'
set xlabel "time (year)"
set ylabel "Tmelt-T (K)" 
set key bottom left

plot \
    "r10m_trs_bedl_m4ep4_3d/scout_secular_r10m_trs_bedl_m4ep4w5v3u6.dat" u 1:($52/$51) w l lw 2 t "3138", \
    "r10m_trs_bedl_m4ep4_3d/scout_secular_r10m_trs_bedl_m4ep4w5v3u6.dat" u 1:($56/$55) w l lw 2 t "3158", \
    "r10m_trs_bedl_m4ep4_3d/scout_secular_r10m_trs_bedl_m4ep4w5v3u6.dat" u 1:($60/$59) w l lw 2 t "3178", \
    "r10m_trs_bedl_m4ep4_3d/scout_secular_r10m_trs_bedl_m4ep4w5v3u6.dat" u 1:($64/$63) w l lw 2 t "3198", \
    "r10m_trs_bedl_m4ep4_3d/scout_secular_r10m_trs_bedl_m4ep4w5v3u6.dat" u 1:($68/$67) w l lw 2 t "3218", \
    "r10m_trs_bedl_m4ep4_3d/scout_secular_r10m_trs_bedl_m4ep4w5v3u6.dat" u 1:($72/$61) w l lw 2 t "3238"

pause -1

